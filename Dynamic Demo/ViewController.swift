//
//  ViewController.swift
//  Dynamic Demo
//
//  Created by Johnny on 4/11/2016.
//  Copyright © 2016 Johnny@Co-fire. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollisionBehaviorDelegate {
    
    let fieldMargin : CGFloat = 300
    let minMagnitude : CGFloat = 8
    let magnitudePadding : CGFloat = 300
    let defaultMagnitude : CGFloat = 12
    let reduceAngularVelocityFactor : Float = 0.8
    
    var animator : UIDynamicAnimator!
    var gravityBehaviour : UIGravityBehavior!
    var collisionBehaviour : UICollisionBehavior!
    var attachmentBehaviour : UIAttachmentBehavior!
    var snapBehaviour : UISnapBehavior!
    var square: UIView!
    var message: UILabel!
    var startpoint = CGPoint()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.animator = UIDynamicAnimator(referenceView: view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        createAlert(text: "Draggable Alert")
    }
    
    func createAlert(text: String) {
        self.square = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 70))
        self.square.layer.cornerRadius = 10
        self.square.clipsToBounds = true
        self.square.center = CGPoint(x: view.center.x, y: 0)
        self.square.backgroundColor = UIColor.black
        self.view.addSubview(square)
        
        self.message = UILabel(frame: CGRect(x: 0, y: 0, width: 250, height: 70))
        self.message.text = text
        self.message.textAlignment = .center
        self.message.textColor = UIColor.white
        self.message.center = CGPoint(x: self.square.frame.size.width/2, y: self.square.frame.size.height/2)
        self.square.addSubview(self.message)
        
        self.square.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(pan)))
        self.square.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        startpoint = self.view.center
        
        addSnap(view: self.square)
    }
    
    func pan(gesture: UIPanGestureRecognizer) {
        let gestureView = gesture.view!
        let dragPoint : CGPoint = gesture.location(in: gestureView)
        
        if gesture.state == UIGestureRecognizerState.began {
            //Start Dragging
            removeSnap(view: gestureView)
            
            let offsetFromCenter: UIOffset = UIOffsetMake(
                dragPoint.x - gestureView.bounds.size.width  / 2.0,
                dragPoint.y - gestureView.bounds.size.height / 2.0
            )
            let anchorPoint: CGPoint = gesture.location(in: gestureView.superview)
            
            attachmentBehaviour = UIAttachmentBehavior(item: gestureView, offsetFromCenter: offsetFromCenter, attachedToAnchor: anchorPoint)
            self.animator.addBehavior(attachmentBehaviour)
        }
        else if gesture.state == UIGestureRecognizerState.changed {
            //Dragging
            let touchPoint : CGPoint = gesture.location(in: gestureView.superview)
            attachmentBehaviour.anchorPoint = touchPoint
            let viewCenter = gestureView.center
            let movedDistance = distance(from: startpoint, to: viewCenter)
            print("Moved Distance = \(movedDistance)")
        }
        else if gesture.state == UIGestureRecognizerState.ended {
            self.animator.removeAllBehaviors()
            
            let viewCenter = gestureView.center
            let movedDistance = distance(from: startpoint, to: viewCenter)
            
            if movedDistance < 50 {
                addSnap(view: gestureView)
            }
            else
            {
                animator.removeAllBehaviors()
                gestureView.removeGestureRecognizer((gestureView.gestureRecognizers?[0])!)
                
                let velocity: CGPoint = gesture.velocity(in: gestureView.superview)
                let vector = CGVector(dx: (velocity.x), dy: (velocity.y))
                
                //Add Push Behaviour
                let pushBehavior = UIPushBehavior(items: [gestureView], mode: UIPushBehaviorMode.instantaneous)
                pushBehavior.pushDirection = vector
                
                let pushMagnitude : CGFloat = pushBehavior.magnitude / magnitudePadding
                
                pushBehavior.magnitude = (pushMagnitude > minMagnitude) ? pushMagnitude : defaultMagnitude
                self.animator.addBehavior(pushBehavior)
                
                //Add Item Behaviour
                let itemBehaviour = UIDynamicItemBehavior(items: [gestureView])
                
                // calculate angles needed for angular velocity formula
                // https://github.com/u10int/URBMediaFocusViewController/blob/master/URBMediaFocusViewController.m#L691-L731
                let offsetFromCenter : UIOffset = UIOffset(horizontal: dragPoint.x - gestureView.bounds.midX, vertical: dragPoint.y - gestureView.bounds.midY)
                let radius = sqrtf(powf(Float(offsetFromCenter.horizontal), 2.0) + powf(Float(offsetFromCenter.vertical), 2.0))
                let velocityAngle : Float = atan2f(Float(velocity.y), Float(velocity.x))
                var locationAngle : Float = atan2f(Float(offsetFromCenter.vertical), Float(offsetFromCenter.horizontal))
                
                if (locationAngle > 0) {
                    locationAngle -= Float(M_PI * 2)
                }
                
                // rotation direction is dependent upon which corner was pushed relative to the center of the view
                // when velocity.y is positive, pushes to the right of center rotate clockwise, left is counterclockwise
                var direction : CGFloat = (dragPoint.x < gestureView.center.x) ? -1.0 : 1.0
                // when y component of velocity is negative, reverse direction
                if (velocity.y < 0) {
                    direction *= -1
                }
                
                // angle (θ) is the angle between the push vector (V) and vector component parallel to radius, so it should always be positive
                let angle : Float = fabs(fabs(velocityAngle) - fabs(locationAngle))
                // angular velocity formula: w = (abs(V) * sin(θ)) / abs(r)
                var angularVelocity : Float = fabs((Float(fabs(pushMagnitude*magnitudePadding)) * sinf(angle)) / fabs(radius))
                
                // amount of angular velocity should be relative to how close to the edge of the view the force originated
                // angular velocity is reduced the closer to the center the force is applied
                // for angular velocity: positive = clockwise, negative = counterclockwise
                let xRatioFromCenter : CGFloat = fabs(offsetFromCenter.horizontal) / (gestureView.bounds.size.width / 2.0)
                let yRatioFromCetner : CGFloat = fabs(offsetFromCenter.vertical) / (gestureView.bounds.size.height / 2.0)
                angularVelocity *= (Float((xRatioFromCenter + yRatioFromCetner)) / 2.0);
                // reduce the angularVelocity (trial & error)
                angularVelocity *= reduceAngularVelocityFactor
                
                itemBehaviour.allowsRotation = true
                itemBehaviour.addAngularVelocity(CGFloat(angularVelocity) * direction, for: gestureView)
                itemBehaviour.action = { [weak self] in self?.removeView(gestureView: gestureView) }
                self.animator.addBehavior(itemBehaviour)
            }
        }
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        tapDismissAlert(gesture: gesture)
    }
    
    func tapDismissAlert(gesture: UITapGestureRecognizer) {
        animator.removeAllBehaviors()
        
        guard let gestureView = gesture.view else { return }
        
        if let gestureViewGestureRecognizers = gestureView.gestureRecognizers {
            for gestureRecongnizers in gestureViewGestureRecognizers {
                gestureView.removeGestureRecognizer(gestureRecongnizers)
            }
        }
        
        gravityBehaviour = UIGravityBehavior(items: [gestureView])
        gravityBehaviour.action = { [weak self] in
            self?.removeView(gestureView: gestureView)
        }
        animator.addBehavior(gravityBehaviour)
    }
    
    func removeView(gestureView: UIView) {
        if gestureView.superview != nil {
            if gestureView.frame.origin.y >= (gestureView.superview!.bounds.origin.y + gestureView.superview!.bounds.size.height + 250) {
                gestureView.removeFromSuperview()
            }
            else if (gestureView.frame.origin.y + gestureView.frame.size.height) <= (gestureView.superview!.bounds.origin.y - 250) {
                gestureView.removeFromSuperview()
            }
            else if (gestureView.frame.origin.x + gestureView.frame.size.width) <= (gestureView.superview!.bounds.origin.x - 250) {
                gestureView.removeFromSuperview()
            }
            else if gestureView.frame.origin.x >= (gestureView.superview!.bounds.origin.x + gestureView.frame.size.width + 250 ) {
                gestureView.removeFromSuperview()
            }
        }
    }
    
    func removeAttachment(view: UIView) {
        animator.removeBehavior(attachmentBehaviour)
    }
    
    func addSnap(view: UIView) {
        snapBehaviour = UISnapBehavior(item: self.square, snapTo: startpoint)
        snapBehaviour.damping = 0.5
        self.animator.addBehavior(snapBehaviour)
    }
    
    func removeSnap(view: UIView) {
        self.animator.removeBehavior(snapBehaviour)
    }
    
    func distance(from:CGPoint, to:CGPoint) -> CGFloat {
        let xDist = (to.x - from.x)
        let yDist = (to.y - from.y)
        return sqrt((xDist * xDist) + (yDist * yDist))
    }
    
    @IBAction func showAlert() {
        createAlert(text: "Next Alert")
    }

}

