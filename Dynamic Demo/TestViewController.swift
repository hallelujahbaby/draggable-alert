//
//  TestViewController.swift
//  Dynamic Demo
//
//  Created by Johnny Choi on 9/11/2016.
//  Copyright © 2016 Johnny@Co-fire. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {
    
    var messager = CFDraggableMessage()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showAlert() {
        messager.showMessage(withText: "hi", viewController: self)
    }
    
    @IBAction func dismissAlert() {
        messager.dismissMessage()
    }

}
